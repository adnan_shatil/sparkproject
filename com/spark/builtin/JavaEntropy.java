package spark.builtin;

import java.util.HashMap;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.tree.DecisionTree;
import org.apache.spark.mllib.tree.model.DecisionTreeModel;
import org.apache.spark.mllib.util.MLUtils;

import scala.Tuple2;

public class JavaEntropy {

	public static void main(String[] args) {
//		String datapath = "data/sample_libsvm_data.txt";
	    
	    SparkConf sparkConf = new SparkConf().setAppName("JavaEntropy").setMaster("local[8]").set("spark.executor.memory","1g");
	    JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);
	
//		JavaRDD<LabeledPoint> data = MLUtils.loadLibSVMFile(javaSparkContext.sc(), datapath).toJavaRDD();
		// Split the data into training and test sets (30% held out for testing)
//		JavaRDD<LabeledPoint>[] splits = data.randomSplit(new double[]{0.7, 0.3});
//		JavaRDD<LabeledPoint> trainingData = splits[0];
//		JavaRDD<LabeledPoint> testData = splits[1];
		
		JavaRDD<LabeledPoint> trainingData = MLUtils.loadLibSVMFile(javaSparkContext.sc(), "data/train.libsvm").toJavaRDD();
		JavaRDD<LabeledPoint> testData = MLUtils.loadLibSVMFile(javaSparkContext.sc(), "data/test.libsvm").toJavaRDD();
	
		// Set parameters.
		//  Empty categoricalFeaturesInfo indicates all features are continuous.
		Integer numClasses = 2;
		HashMap<Integer, Integer> categoricalFeaturesInfo = new HashMap<Integer, Integer>();
		String impurity = "gini";
		Integer maxDepth = 5;
		Integer maxBins = 10;
	
		// Train a DecisionTree model for classification.
		final DecisionTreeModel model = DecisionTree.trainClassifier(trainingData, numClasses,
		  categoricalFeaturesInfo, impurity, maxDepth, maxBins);
	
		// Evaluate model on test instances and compute test error
		JavaPairRDD<Double, Double> predictionAndLabel =
		  testData.mapToPair(new PairFunction<LabeledPoint, Double, Double>() {
		    @Override
		    public Tuple2<Double, Double> call(LabeledPoint p) {
		      return new Tuple2<Double, Double>(model.predict(p.features()), p.label());
		    }
		  });
		
		Double testErr =
		  1.0 * predictionAndLabel.filter(new Function<Tuple2<Double, Double>, Boolean>() {
		    @Override
		    public Boolean call(Tuple2<Double, Double> pl) {
		      return !pl._1().equals(pl._2());
		    }
		  }).count() / testData.count();
		
		System.out.println("Test Error: " + testErr);
		System.out.println("Learned classification tree model:\n" + model.toDebugString());
		
		javaSparkContext.stop();
	}

}
