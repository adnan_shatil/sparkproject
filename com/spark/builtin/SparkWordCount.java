package spark.builtin;

import java.util.Arrays;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class SparkWordCount {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SparkConf sparkConf = new SparkConf().setAppName("SparkWordCount").setMaster("local[8]").set("spark.executor.memory","1g");
	    JavaSparkContext sc = new JavaSparkContext(sparkConf);
		
		JavaRDD<String> textFile = sc.textFile("data/wordcount.txt");
		
		JavaRDD<String> words = textFile.flatMap(
			new FlatMapFunction<String, String>() {
				public Iterable<String> call(String s) { 
					s = s.replaceAll("[^a-zA-Z\\s]", "").replaceAll("\\s+", " ");
					return Arrays.asList(s.split(" ")); 
				}
			}
		);
		
		JavaPairRDD<String, Integer> pairs = words.mapToPair(
			new PairFunction<String, String, Integer>() {
				public Tuple2<String, Integer> call(String s) { 
					return new Tuple2<String, Integer>(s, 1); 
				}
			}
		);
		
		JavaPairRDD<String, Integer> counts = pairs.reduceByKey(
			new Function2<Integer, Integer, Integer>() {
				public Integer call(Integer a, Integer b) { 
					return a + b; 
				}
			}
		);
		
		counts.saveAsTextFile("data/outdata/stat");
	}

}
