package spark.ml;

import org.apache.spark.SparkConf;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SparkConf conf = new SparkConf().setAppName("MainClass").setMaster("local[8]").set("spark.executor.memory","1g");
		
		TestLogisticRegression testLogisticRegression = new TestLogisticRegression(conf);
		testLogisticRegression.runTest();
		
//		TestLDA testLDA = new TestLDA(conf);
//		testLDA.runTest();
		
//		TestGini testGini = new TestGini(conf);
//		testGini.runTest();
//		
//		TestEntropy testEntropy = new TestEntropy(conf);
//		testEntropy.runTest();
		
	}

}
